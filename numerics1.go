package main

import (
    "fmt"
)

func main () {
    a, b := 5.87, 6.89
    fmt.Printf("A: %f, B: %f\n", a, b)
    fmt.Printf("Sum: %f, Diff: %f\n", a+b, a-b)
}